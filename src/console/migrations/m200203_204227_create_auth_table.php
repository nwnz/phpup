<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auth}}`.
 */
class m200203_204227_create_auth_table extends Migration
{
    public function up()
{

    $this->createTable('auth', [
        'id' => $this->primaryKey(),
        'user_id' => $this->integer()->notNull(),
        'source' => $this->string()->notNull(),
        'source_id' => $this->string()->notNull(),
    ]);

    $this->addForeignKey('vk-auth-user_id-user-id', 'auth', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
}

    public function down()
{
    $this->dropTable('auth');
}
}
